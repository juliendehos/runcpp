// example1.cpp
// a simple C++ script 

#include<iostream>

using namespace std;

int main(int argc, char ** argv) 
{
    if (argc != 2)
    {
        cerr << "usage: " << argv[0] << " <your name>" << endl;
        exit(-1);
    }
    cout << "Hello " << argv[1] << endl;
    return 0;
}

