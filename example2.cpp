// example2.cpp
// a C++ script requiring compilation options
//
// RUNCPP_BUILD -lboost_filesystem -lboost_system
// RUNCPP_BUILD $(pkg-config --cflags --libs gtkmm-2.4)
// RUNCPP_RUN .

#include <boost/filesystem.hpp>
#include <gtkmm.h>
#include <iostream>

using namespace boost::filesystem;
using namespace std;

int main(int argc, char ** argv) 
{
    if (argc != 2)
    {
        cout << argc << endl;
        cout << "usage: " << argv[0] << " <path>" << endl;
        exit(-1);
    }
    path someDir(argv[1]);
    directory_iterator end_iter;
    for( directory_iterator dir_iter(someDir) ; dir_iter != end_iter ; ++dir_iter)
        cout << dir_iter->path().filename().string() << endl;
    return 0;
}
