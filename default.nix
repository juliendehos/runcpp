{ pkgs ? import <nixpkgs> {} }:

pkgs.stdenv.mkDerivation {
  name = "runcpp";
  src = ./.;

  buildPhase = "cp $src/runcpp.sh runcpp";

  installPhase = ''
    mkdir -p $out/bin
    cp runcpp $out/bin/
  '';
}

