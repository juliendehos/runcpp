# runcpp
run a simple C++ code as a script

## install
```
sudo make install
```

## run
```
$ runcpp example2.cpp 
README.md
...
$ runcpp example1.cpp Julien
Hello Julien
$ CXX=clang++ CXXFLAGS="-O2" runcpp example1.cpp Julien
Hello Julien
```

Specific compilation options and default run arguments can be set in the C++ script (see example2.cpp) :
```
// RUNCPP_BUILD <build options>
// RUNCPP_RUN <default run arguments>
```

