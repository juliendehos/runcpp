#!/bin/sh

# compile and run a C++ file
# use RUNCPP_BUILD to specify build parameters
# use RUNCPP_RUN to specify run arguments
# documentation: https://github.com/juliendehos/runcpp

# check that a C++ file is given
if [ $# -lt 1 ]
then
    echo "usage: $@ <cpp filename> [params...]"
    exit
fi

# source file, binary file and run arguments
src_name="$1"
bin_name="${src_name}.runcpp.out"
shift
run_args="$@"

# set compiler if not defined in environment
if [ -s ${CXX} ]
then
    CXX="c++"
fi

# set compile flags if not defined in environment
if [ -s ${CXXFLAGS} ]
then
    CXXFLAGS="-std=c++14 -Wall -Wextra"
fi

# set link flags if not defined in environment
if [ -s ${LDFLAGS} ]
then
    LDFLAGS=""
fi

# get additional build options from the C++ file
OPT_BUILD="`sed -n 's/^.*RUNCPP_BUILD //p' ${src_name} | tr '\n' ' '`"

# use run arguments from the command line
# or from the C++ file if no argument in the command line
if [ -z "${run_args}" ]
then
    OPT_RUN="`sed -n 's/^.*RUNCPP_RUN //p' ${src_name} | tr '\n' ' '`"
else
    OPT_RUN="${run_args}"
fi

# compile and run
eval ${CXX} ${CXXFLAGS} -o ${bin_name} ${LDFLAGS} ${OPT_BUILD} ${src_name} && ./${bin_name} ${OPT_RUN}

# remove temporary file (binary file)
rm -f ${bin_name}

